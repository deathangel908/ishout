
# iShout.js

iShout.js is a Node.js + Socket.io server used for adding real-time push notifications to your existing web application.

## what does that mean exactly? 

Say you have an awesome web app you developed in Django\Rails\PHP\Perl\Java\Bash\Awk.
You one day decide you want to add support for push (real-time) notifications.
Sure, you can run periodic AJAX calls, do long-polling, or utilize WebSockets yourself.
But that's a lot of work, and that's what [Socket.IO](http://socket.io/ "Socket.IO") is for.

iShout.js lets you easily integrate your existing framework with the power of socket.io.
just install it, add the proper client for your web framework of choice (currently, it's Django only),
and send those realtime messages from your own code, in your own language, inside your own views.
iShout.js will take care of distribution (which sockets should I send data to?) and authentication
(how can I securely map these sockets to my users?).

## Awesome, How does it work?

Well, it's basically a proxy server. when your web app calls iShout.js, it does so over an internal HTTP API.
On the client side, your client is also connected to the iShout.js sever, using socket.io.
So when you, inside your webapp, send a request (lets say, send a message "you rock!" to user A)
the iShout.js API takes that request, finds the appropriate socket(s) for a client called "A", and emits
that message. that's the basic workflow.

## Dependencies

iShout.js requires the following software installed and configured:

* [Node.js](http://nodejs.org/ "Node.js")
* [NPM](http://npmjs.org/ "NPM") (automatically installs all 3rd party libraries this project depends on.)
* [Redis](http://redis.io/ "Redis") (optional, but highly recommended. see *Configuration* below).


## Installation

installing is pretty simple.

```bash
npm install ishout.js
```


## Run the server

```bash
node node_modules/ishout.js/server.js
```


## Configuration
**changed in 0.2.3** 

Configuring announce is done by providing a JSON configuration file as a command line argument.
To specify the path to this file, use the `--config` command line option, like so:

```bash
node server.js --config=/path/to/settings.json
```

For local development, the above is not required as iShout.js has some pretty sane defaults.
in any case, these configuration parameters are supported:

* `storage` - which storage backend to use. options are either `mem` or `redis`. defaults to `redis`, which also happens to be the preffered backend. the local memory backend should only be used for testing.
* `redisHost` - the host used for Redis connections. defaults to `'localhost'`.
* `redisPort` - the port used for Redis connections. defaults to `6379`.
* `redisPassword` - set this to your Redis password, if your Redis server requires one.
* `apiHost` - the host to listen on for the internal API. this should be the same value used in your webapp to connect to iShout.js. defaults to `'localhost'`.
* `apiPort` - the port to listen on for the internal API. this should be the same value used in your webapp to connect to iShout.js. defaults to `6600`.
* `socketHost` - the host to listen on for the external socket.IO server. defaults to `'0.0.0.0'` (so it will be available from the "outside").
* `socketPort` - the port to listen on for the external socket.IO server. this should be open in your firewall for traffic coming in from the internet. defaults to `5500`.
* `sslKey` - path to an optional SSL key file. Add this and `sslCertificate` if you want to serve iShout.js over HTTPS.
* `sslCertificate` - path to an SSL certificate file. the server will start in SSL mode only if both `sslKey` and `sslCertificate` are provided, and both are valid. else, it will start in regular, unencrypted HTTP. so pay attention.
* `socketsVolatile` - whether or not to buffer messages when sending to a client. read more about volatile mode [here](https://github.com/LearnBoost/socket.io/wiki/Messaging "Socket.IO wiki"). defaults to `false`.



## Authorization


The iShout.js authorization model works like this:

1. client A makes a request to your webapp.
2. your webapp turns to iShout.js's internal API and requests a token for that user's ID.
3. upon receiving the token, the webapp sets a cookie called `iShoutToken` to the value of the token, and renders the requested page back to client A.
4. the requested page (containing the iShout.js javascript include) uses this cookie to retrieve the token, and validates it against the iShout.js server.
5. upon successful validation, a connection is established and your client will start listening on channels and events you define.

These steps are all handled by your framework's iShout.js client. you just need to install ishout,
include the javascript client on your page, and start the iShout.js server.

